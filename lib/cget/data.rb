require 'cget/data/version'
require 'httparty'
require 'nokogiri'
require 'json'
require 'csv'

module Cget
  module Data
    SITES = %I{resolis marie_conseils fondation_vinci fondation_veolia
               imagination_for_people unccas experimentation_jeunes apriles
               social_planet semeoz bretagne_creative bleublanczebre economiecirculaire
               fondationrte lelaboess}.freeze

    # Retrieves projects from all the available sites
    #
    # @param [Array<Symbol>] sites to be visited (default: all available sites)
    # @return [Array<Cget::Data::Project>]
    def self.find(sites: SITES, limit: Float::INFINITY, exclude: lambda { |url, site| false }, process: lambda { |project| true })
      (sites & SITES).map do |site|
        # rubocop:disable Eval
        site_klass = eval("Sites::#{site.to_s.split('_').map(&:capitalize).join('')}") rescue nil
        next unless site_klass
        site_instance = site_klass.new
        site_instance.find(limit: limit, exclude: exclude, process: process)
      end.flatten
    end

    autoload :Project, 'cget/data/project'

    module Operations
      autoload :Base, 'cget/data/operations/base'
      autoload :Find, 'cget/data/operations/find'
    end

    module Sites
      autoload :Base, 'cget/data/sites/base'
      autoload :BaseJson, 'cget/data/sites/base_json'
      autoload :Resolis, 'cget/data/sites/resolis'
      autoload :MarieConseils, 'cget/data/sites/marie_conseils'
      autoload :FondationVinci, 'cget/data/sites/fondation_vinci'
      autoload :FondationVeolia, 'cget/data/sites/fondation_veolia'
      autoload :ImaginationForPeople, 'cget/data/sites/imagination_for_people'
      autoload :Unccas, 'cget/data/sites/unccas'
      autoload :ExperimentationJeunes, 'cget/data/sites/experimentation_jeunes'
      autoload :Apriles, 'cget/data/sites/apriles'
      autoload :SocialPlanet, 'cget/data/sites/social_planet'
      autoload :Semeoz, 'cget/data/sites/semeoz'
      autoload :BretagneCreative, 'cget/data/sites/bretagne_creative'
      autoload :Bleublanczebre, 'cget/data/sites/bleublanczebre'
      autoload :Economiecirculaire, 'cget/data/sites/economiecirculaire'
      autoload :Fondationrte, 'cget/data/sites/fondationrte'
      autoload :Lelaboess, 'cget/data/sites/lelaboess'
    end
  end
end
