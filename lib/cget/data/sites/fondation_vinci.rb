module Cget
  module Data
    module Sites
      class FondationVinci < Base
        @configuration = {
          base_url: 'https://www.fondation-vinci.com/fondation/fr/recherche/projets.htm&a=&r=&d=&t=&g=&p=&c=&q=#form',
          project_url_selector: 'div.main-wrapper div.projets div.icono > a',
          project_title_selector: 'div.main-wrapper div.content.chapter div.chapeauText > h2',
          project_description_selector: 'div.main-wrapper div.content.chapter > div > div.column.column2 > p, body > div.main-wrapper > div.content.chapter.chapter02 h3:contains("Project") + p, body > div.main-wrapper > div.content.chapter.chapter02 h3:contains("Contexte") + p',
          project_email_selector: '.article-fiche-header [itemprop="author"] [itemprop="email"]',
          project_contact_selector: 'div.main-wrapper div.content.chapter div.column.column3.aplatgris.floatRight > p:contains("Structure"), body > div.main-wrapper > div.content.chapter.chapter02 h3:contains("Coordonnées de la structure") + p',
          project_address_selector: 'div.main-wrapper div.content.chapter div.column.column3.aplatgris.floatRight > p:contains("Structure"), body > div.main-wrapper > div.content.chapter.chapter02 h3:contains("Coordonnées de la structure") + p',
          project_city_selector: 'div.main-wrapper div.content.chapter div.chapeauText > h2 + p > strong'
        }

        protected

        def max_pages
          1
        end

        def page_url
          configuration[:base_url]
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_email(parsed_content)
          extract_email_from(:contact, parsed_content) ||
          extract_email_from(:address, parsed_content) ||
          extract_email_from(:description, parsed_content)
        end
      end
    end
  end
end
