module Cget
  module Data
    module Sites
      class Resolis < Base
        @configuration = {
          base_url: 'http://www.resolis.org/consulter-les-pratiques-locales/solution/agriculture-et-alimentation+coordination-des-actions+culture-sport-et-loisirs+democratie-et-bonne-gouvernance+developpement-territorial+economie-solidaire+education+emploi+environnement+exclusion-et-isolement+logement+mobilite+precarite-energetique+sante',
          project_url_selector: '.article-consulter .mod a.btn',
          max_page_selector: '.pagination ul li:not(.next) a',
          project_title_selector: '.article-fiche-header .w100-fiche h1',
          project_description_selector: '.article-fiche-header .w100-fiche p span.field',
          project_email_selector: '.article-fiche-header [itemprop="author"] [itemprop="email"]',
          project_contact_selector: '.article-fiche-header [itemprop="creator"] [itemprop="location"]',
          project_address_selector: '.article-fiche-header [itemprop="creator"] [itemprop="location"]',
          project_tags_selector: '.article-fiche-comite .label-solution .comite-label',
          project_city_selector: '.article-fiche-header [itemprop="contentLocation"]'
        }

        protected

        def page_url
          configuration[:base_url] + "/#{@current_page}"
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_email(parsed_content)
          extract_email_from(:contact, parsed_content) ||
          extract_email_from(:address, parsed_content) ||
          extract_email_from(:description, parsed_content)
        end
      end
    end
  end
end
