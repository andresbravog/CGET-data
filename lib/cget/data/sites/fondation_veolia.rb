module Cget
  module Data
    module Sites
      class FondationVeolia < Base
        @configuration = {
          base_url: 'http://www.fondation.veolia.com/fr/projets-soutenus?date_filter%5Bvalue%5D%5Byear%5D=&country=All&keywords=&domain=All&op=Rechercher',
          project_url_selector: '#read > div.listcp-content div.block-doc-downloads-content > div.doc-item-press > a',
          max_page_selector: '#read > div.listcp-content > div:nth-child(3) > div > ul > li.pager-last.last > a',
          project_title_selector: '#read > h1',
          project_description_selector: '#read > blockquote > p',
          project_city_selector: '#read > div > div > div > div.block-links.block-links-vertical.pull-right.ble-green > div > ul > li > span:contains("Lieu") + p'
        }

        protected

        # Creates project url based on the given project url
        # buy adding the base ulr if project url is a relative one
        #
        # @param project_url [String] extracted project url
        # @return [String] complete project url
        def build_project_url(project_url)
          return project_url if project_url =~ /^(https?|ftp):/
          'http://www.fondation.veolia.com' + project_url
        end

        def page_url
          configuration[:base_url] + "&page=#{@current_page - 1}"
        end

        def extract_email(parsed_content)
          extract_email_from(:description, parsed_content)
        end
      end
    end
  end
end
