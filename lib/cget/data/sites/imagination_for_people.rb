# http://imaginationforpeople.org/fr/search/
module Cget
  module Data
    module Sites
      class ImaginationForPeople < Base
        @configuration = {
          base_url: 'http://imaginationforpeople.org/fr/projets/search/?language_codes=fr',
          project_url_selector: '#projects-list div.hover > div.more > a',
          max_page_selector: '#main > div:nth-child(1) > div > a:nth-last-child(2)',
          project_title_selector: '#subheader-title-1',
          project_description_selector: '#about-description-1',
          project_contact_selector: 'body > div.global > div.project-subheader.readonly > div > div > div.span4.infocard > table > tbody > tr.infotable-website > td > a',
          project_address_selector: '.infocard .infotable-location.location .location-address',
          project_city_selector: '.infocard .infotable-location.location .location-country'
        }

        protected

        # Creates project url based on the given project url
        # buy adding the base ulr if project url is a relative one
        #
        # @param project_url [String] extracted project url
        # @return [String] complete project url
        def build_project_url(project_url)
          return project_url if project_url =~ /^(https?|ftp):/
          'http://imaginationforpeople.org' + project_url
        end

        def page_url
          configuration[:base_url] + "&page=#{@current_page}"
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_email(parsed_content)
          extract_email_from(:contact, parsed_content) ||
          extract_email_from(:address, parsed_content) ||
          extract_email_from(:description, parsed_content)
        end
      end
    end
  end
end
