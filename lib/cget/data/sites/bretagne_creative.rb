# http://www.bretagne-creative.net/rubrique2.html?theme=&debut_articles=0#pagination_articles
module Cget
  module Data
    module Sites
      class BretagneCreative < Base
        @configuration = {
          base_url: 'http://www.bretagne-creative.net/',
          project_url_selector: 'a[rel="bookmark"]',
          project_title_selector: '[class*="article-titre-"]',
          project_description_selector: '.content-principal',
          project_picture_selector: '[class*="article-logo-"] > img',
          project_contact_selector: '.menu.cartouche'
        }

        protected

        def max_pages
          6
        end

        def page_url
          configuration[:base_url] + "rubrique2.html?theme=&debut_articles=#{(@current_page - 1) * 30}#pagination_articles"
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          return unless title
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_email(parsed_content)
          extract_email_from(:description, parsed_content)
          extract_email_from(:contact, parsed_content)
        end

      end
    end
  end
end
