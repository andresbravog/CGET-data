REGIONS = {
  'Ain' => '01',
  'Aisne' => '02',
  'Allier' => '03',
  'Alpes-de-Haute-Provence' => '04',
  'Hautes-Alpes' => '05',
  'Alpes-Maritimes' => '06',
  'Ardèche' => '07',
  'Ardennes' => '08',
  'Ariège' => '09',
  'Aube' => '10',
  'Aude' => '11',
  'Aveyron' => '12',
  'Bouches-du-Rhône' => '13',
  'Calvados' => '14',
  'Cantal' => '15',
  'Charente' => '16',
  'Charente-Maritime' => '17',
  'Cher' => '18',
  'Corrèze' => '19',
  'Haute-Corse' => '2B',
  'Corse-du-Sud' => '2A',
  'Côte-d\'Or' => '21',
  'Côtes-d\'Armor' => '22',
  'Creuse' => '23',
  'Dordogne' => '24',
  'Doubs' => '25',
  'Drôme' => '26',
  'Eure' => '27',
  'Eure-et-Loir' => '28',
  'Finistère' => '29',
  'Gard' => '30',
  'Haute-Garonne' => '31',
  'Gers' => '32',
  'Gironde' => '33',
  'Hérault' => '34',
  'Ille-et-Vilaine' => '35',
  'Indre' => '36',
  'Indre-et-Loire' => '37',
  'Isère' => '38',
  'Jura' => '39',
  'Landes' => '40',
  'Loir-et-Cher' => '41',
  'Loire' => '42',
  'Haute-Loire' => '43',
  'Loire-Atlantique' => '44',
  'Loiret' => '45',
  'Lot' => '46',
  'Lot-et-Garonne' => '47',
  'Lozère' => '48',
  'Maine-et-Loire' => '49',
  'Manche' => '50',
  'Marne' => '51',
  'Haute-Marne' => '52',
  'Mayenne' => '53',
  'Meurthe-et-Moselle' => '54',
  'Meuse' => '55',
  'Morbihan' => '56',
  'Moselle' => '57',
  'Nièvre' => '58',
  'Nord' => '59',
  'Oise' => '60',
  'Orne' => '61',
  'Pas-de-Calais' => '62',
  'Puy-de-Dôme' => '63',
  'Pyrénées-Atlantiques' => '64',
  'Hautes-Pyrénées' => '65',
  'Pyrénées-Orientales' => '66',
  'Bas-Rhin' => '67',
  'Haut-Rhin' => '68',
  'Rhône' => '69',
  'Haute-Saône' => '70',
  'Saône-et-Loire' => '71',
  'Sarthe' => '72',
  'Savoie' => '73',
  'Haute-Savoie' => '74',
  'Paris' => '75',
  'Seine-Maritime' => '76',
  'Seine-et-Marne' => '77',
  'Yvelines' => '78',
  'Deux-Sèvres' => '79',
  'Somme' => '80',
  'Tarn' => '81',
  'Tarn-et-Garonne' => '82',
  'Var' => '83',
  'Vaucluse' => '84',
  'Vendée' => '85',
  'Vienne' => '86',
  'Haute-Vienne' => '87',
  'Vosges' => '88',
  'Yonne' => '89',
  'Territoire-de-Belfort' => '90',
  'Essonne' => '91',
  'Hauts-de-Seine' => '92',
  'Seine-Saint-Denis' => '93',
  'Val-de-Marne' => '94',
  'Val-d\'Oise' => '95',
  'Autre pays' => '99',
  'Transfrontalier' => '100',
  'Guadeloupe' => '971',
  'Martinique' => '972',
  'Guyane' => '973',
  'Réunion' => '974',
  'Saint-Pierre-et-Miquelon' => '975',
  'Mayotte' => '976'
}.freeze

module Cget
  module Data
    module Sites
      class MarieConseils < Base
        @configuration = {
          base_url: 'http://www.mairieconseils.net/cs/ContentServer?pagename=Mairie-conseils/Page/ToutesLesExperiences',
          project_url_selector: '#listeTableau > tbody > tr > td.first > span > a',
          project_title_selector: '#zyyne-titre',
          project_description_selector: '#zyyne-texte',
          project_email_selector: '#zyyne-contact > div > div > div.contact > span:nth-child(4) > a',
          project_contact_selector: '#zyyne-contact',
          project_address_selector: '#zyyne-contact > div > div > div.adresse',
          project_tags_selector: '#zyyne-rubrique > a',
          project_city_selector: '.article-fiche-header [itemprop="contentLocation"]'
        }

        protected

        def max_pages
          REGIONS.to_a.size
        end

        # Increases the current page and gives the url of the next one if
        # there is any
        #
        # @return [String or Nil] Next page url
        def next_page_url
          @current_page ||= REGIONS.to_a.first.last
          return if @current_page == REGIONS.to_a.last.last
          values = REGIONS.map(&:last)
          @current_page = values[values.index(@current_page) + 1]
          page_url
        end

        def page_url
          configuration[:base_url] + "&code=#{@current_page}"
        end

        # Creates project url based on the given project url
        # buy adding the base ulr if project url is a relative one
        #
        # @param project_url [String] extracted project url
        # @return [String] complete project url
        def build_project_url(project_url)
          return project_url if project_url =~ /^(https?|ftp):/
          'http://www.mairieconseils.net' + '/' + project_url
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:title, parsed_content)
          match = title.match(/\(([0-9]{1,3})\)/)
          return unless match
          precode = match.values_at(1).first
          "#{precode}#{'0' * (5-precode.size)}"
        end

        def extract_email(parsed_content)
          extract_email_from(:contact, parsed_content) ||
          extract_email_from(:address, parsed_content) ||
          extract_email_from(:description, parsed_content)
        end
      end
    end
  end
end
