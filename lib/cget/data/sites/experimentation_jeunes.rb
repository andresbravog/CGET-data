# http://www.experimentation.jeunes.gouv.fr/spip.php?page=recherche-exp&&per_page=106
module Cget
  module Data
    module Sites
      class ExperimentationJeunes < Base
        @configuration = {
          base_url: 'http://www.experimentation.jeunes.gouv.fr/spip.php?page=recherche-exp',
          project_url_selector: '#content a.lirelasuite',
          project_title_selector: '#content > div.article-wrapper > h2',
          project_description_selector: '#content > div.cnt',
          project_tags_selector: '#content tr > td:contains("Thématique") > b',
          project_city_selector: '#content tr > td:contains("Région") > b'
        }

        protected

        # Creates project url based on the given project url
        # buy adding the base ulr if project url is a relative one
        #
        # @param project_url [String] extracted project url
        # @return [String] complete project url
        def build_project_url(project_url)
          return project_url if project_url =~ /^(https?|ftp):/
          'http://www.experimentation.jeunes.gouv.fr/' + project_url
        end

        def max_pages
          107
        end

        def page_url
          configuration[:base_url] + "&per_page=#{@current_page-1}"
        end
      end
    end
  end
end
