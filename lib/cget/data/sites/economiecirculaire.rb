# http://www.economiecirculaire.org/initiative/#page1:local
module Cget
  module Data
    module Sites
      class Economiecirculaire < BaseJson
        @configuration = {
          base_url: 'http://www.economiecirculaire.org/rex/datasrc/rex.list.json',
          project_data_selector: '["records"]',
          project_title_selector: '["title"]',
          project_address_selector: '["address"]',
          project_contact_selector: '["address"]',
          project_link_selector: '["href"]',
          project_description_selector: '["subtitle"]',
          project_picture_selector: '["img_path"]',
          project_tags_selector: '["tags"]',
          project_lat_selector: '["address_Lat"]',
          project_long_selector: '["address_Lng"]'
        }

        protected

        def max_pages
          1
        end

        def page_url
          configuration[:base_url]
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          return unless title
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_email(parsed_content)
          extract_email_from(:description, parsed_content)
          extract_email_from(:contact, parsed_content)
        end

      end
    end
  end
end
