module Cget
  module Data
    module Sites
      # rubocop:disable Metrics/ClassLength
      class BaseJson
        @configuration = {
          base_url: 'http://siteurl.com',
          projects_data_selector: '["records"]'
        }
        class << self
          attr_reader :configuration
        end

        attr_accessor :current_page, :projects_data, :projects, :options

        def find(options = {})
          @options = options
          extract_projects_data
          @projects || []
        end

        private

        def configuration
          self.class.configuration
        end

        def extract_projects(projects_data = nil)
          @projects ||= []
          @projects += (projects_data || @projects_data || []).map do |project_data|
            project = extract_project(project_data)
            options[:process].call(project) if options[:process]
            project
          end
        end

        def extract_project(project_datum)
          project_data = parse_project_content(project_datum)
          project_data[:link] = project_data[:link]
          project_data[:site] = self.class.to_s
          Cget::Data::Project.new(project_data)
        end

        def extract_projects_data
          while new_data = next_page_projects_data
            @projects_data.uniq!
            @projects_data.reject! { |data| options[:exclude].call(data[:id], self.class.to_s) } if options[:exclude]
            @projects_data = @projects_data.first(max_projects) if @projects_data.size > max_projects
            extract_projects(@projects_data & new_data)
            break unless @projects_data.size < max_projects
          end
          @projects_data
        end

        def max_projects
          (options[:limit] || Float::INFINITY)
        end

        def next_page_projects_data
          @projects_data ||= []
          content = next_page_content
          return unless content
          new_projects_data = parse_projects_data(content)
          @projects_data += new_projects_data
          new_projects_data
        end

        def next_page_content
          extract_url_content(next_page_url)
        end

        def extract_url_content(url)
          return unless url
          HTTParty.get(url) rescue HTTParty.get(URI.encode(url))
        end

        # Memoized max number of pages for the projects list
        # To be overwriten in every Site template if needed
        #
        # @return [Integer] Number of projects
        def max_pages
          1
        end

        # Gives the current_page url
        # To be overwriten in every Site template if needed
        #
        # @return [String] Current page url
        def page_url
          configuration[:base_url] + "?page=#{@current_page}"
        end

        # Increases the current page and gives the url of the next one if
        # there is any
        #
        # @return [String or Nil] Next page url
        def next_page_url
          @current_page ||= 0
          return unless @current_page < max_pages
          @current_page += 1
          page_url
        end

        # Parse the given html content and extracts the project url
        #
        # @param [String] html content
        # @return [Array<String>] project details urls
        def parse_projects_data(content = '')
          return if content.empty?
          parsed_content = JSON.parse(content)
          eval("parsed_content#{configuration[:project_data_selector]}")
        end

        # Parse the given project html content and extracts the project data
        #
        # - title
        # - link
        # - tags
        # - description
        # - email contact
        # - location (address, city, zipcode)
        #
        # @param [String] html content
        # @return [Hash] project attributes
        def parse_project_content(content = '')
          return {} if content.empty?
          parsed_content = content
          project_attributes = [:title, :link, :tags, :description, :site, :email,
                                :contact, :address, :city, :zipcode, :lat, :long, :picture]
          project_attributes.each_with_object({}) do |project_attribute, out|
            value = parse_project_attribute(project_attribute, parsed_content)
            out[project_attribute] = value unless value.nil?
            out
          end
        end

        # rubocop:disable Metrics/MethodLength
        # rubocop:disable Metrics/AbcSize
        def parse_project_attribute(project_attribute, parsed_content)
          new_parsed_content = parsed_content.clone
          selector = configuration["project_#{project_attribute}_selector".to_sym]
          unless selector.nil?
            value = eval("new_parsed_content#{selector}")
          end
          if methods.include?("extract_#{project_attribute}".to_sym)
            value ||= send("extract_#{project_attribute}", new_parsed_content)
          end

          value
        end

        def extract_email_from(attribute, parsed_content)
          content = parse_project_attribute(attribute, parsed_content)
          return unless content
          content.scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i).first
        end
      end
    end
  end
end
