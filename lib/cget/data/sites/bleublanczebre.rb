# http://www.bleublanczebre.fr/je-decouvre/toutes-les-actions?q=&t=&page=2
module Cget
  module Data
    module Sites
      class Bleublanczebre < Base
        @configuration = {
          base_url: 'http://www.bleublanczebre.fr/je-decouvre/toutes-les-actions',
          project_url_selector: '.tab .contenu .contenu_header > a',
          project_title_selector: '.wrap_title h1',
          project_description_selector: '.wrapper_inside',
          project_picture_selector: '[class*="article-logo-"] > img',
          project_tags_selector: '.thematique'
        }

        protected

        def max_pages
          29
        end

        def page_url
          configuration[:base_url] + "?q=&t=&page=#{@current_page}"
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          return unless title
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_email(parsed_content)
          extract_email_from(:description, parsed_content)
          extract_email_from(:contact, parsed_content)
        end

      end
    end
  end
end
