# http://www.unccas.org/-banque-d-experience-?debut_articles=10#pagination_articles
module Cget
  module Data
    module Sites
      class Semeoz < Base
        @configuration = {
          base_url: 'http://semeoz.info/annuaire/',
          project_url_selector: 'a.post-thumbnail',
          max_page_selector: '.pagination > a.page-numbers:not(.next)',
          project_title_selector: '.entry-title',
          project_description_selector: '.entry-content',
          project_picture_selector: '.entry-content .wp-post-image',
          project_tags_selector: '.taxonomy a'
        }

        protected

        def page_url
          configuration[:base_url] + "page/#{@current_page}/"
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          return unless title
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_email(parsed_content)
          extract_email_from(:description, parsed_content)
        end

      end
    end
  end
end
