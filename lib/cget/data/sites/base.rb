module Cget
  module Data
    module Sites
      # rubocop:disable Metrics/ClassLength
      class Base
        @configuration = {
          base_url: 'http://siteurl.com',
          project_url_selector: '.project a',
          max_page_selector: '.pages.last'
        }
        class << self
          attr_reader :configuration
        end

        attr_accessor :current_page, :project_urls, :projects, :options

        def find(options = {})
          @options = options
          extract_project_urls
          @projects || []
        end

        private

        def configuration
          self.class.configuration
        end

        def extract_projects(project_urls = nil)
          @projects ||= []
          @projects += (project_urls || @project_urls || []).map do |project_url|
            project = extract_project(project_url)
            options[:process].call(project) if options[:process]
            project
          end
        end

        def extract_project(project_url)
          url = build_project_url(project_url)
          content = extract_url_content(url)
          project_data = parse_project_content(content)
          project_data[:link] = url
          project_data[:site] = self.class.to_s
          Cget::Data::Project.new(project_data)
        end

        def extract_project_urls
          while new_urls = next_page_project_urls
            @project_urls.uniq!
            @project_urls.reject! { |url| options[:exclude].call(build_project_url(url), self.class.to_s) } if options[:exclude]
            @project_urls = @project_urls.first(max_projects) if @project_urls.size > max_projects
            extract_projects(@project_urls & new_urls)
            break unless @project_urls.size < max_projects
          end
          @project_urls
        end

        def max_projects
          (options[:limit] || Float::INFINITY)
        end

        def next_page_project_urls
          @project_urls ||= []
          content = next_page_content
          return unless content
          new_project_urls = parse_project_urls(content)
          @project_urls += new_project_urls
          new_project_urls
        end

        def next_page_content
          extract_url_content(next_page_url)
        end

        def extract_url_content(url)
          return unless url
          HTTParty.get(url) rescue HTTParty.get(URI.encode(url))
        end

        # Memoized max number of pages for the projects list
        # To be overwriten in every Site template if needed
        #
        # @return [Integer] Number of projects
        def max_pages
          return @max_pages_number if @max_pages_number
          content = HTTParty.get(configuration[:base_url])
          parsed_content = Nokogiri::HTML(content)
          @max_pages_number = parsed_content.css(configuration[:max_page_selector]).last.text.to_i
        end

        # Gives the current_page url
        # To be overwriten in every Site template if needed
        #
        # @return [String] Current page url
        def page_url
          configuration[:base_url] + "?page=#{@current_page}"
        end

        # Increases the current page and gives the url of the next one if
        # there is any
        #
        # @return [String or Nil] Next page url
        def next_page_url
          @current_page ||= 0
          return unless @current_page < max_pages
          @current_page += 1
          page_url
        end

        # Parse the given html content and extracts the project url
        #
        # @param [String] html content
        # @return [Array<String>] project details urls
        def parse_project_urls(content = '')
          return if content.empty?
          parsed_content = Nokogiri::HTML(content)
          parsed_content.css(configuration[:project_url_selector]).map do |a|
            a.attributes['href'].value rescue a.text
          end
        end

        # Parse the given project html content and extracts the project data
        #
        # - title
        # - link
        # - tags
        # - description
        # - email contact
        # - location (address, city, zipcode)
        #
        # @param [String] html content
        # @return [Hash] project attributes
        def parse_project_content(content = '')
          return {} if content.empty?
          parsed_content = Nokogiri::HTML(content)
          project_attributes = [:title, :link, :tags, :description, :site, :email,
                                :contact, :address, :city, :zipcode, :lat, :long, :picture]
          project_attributes.each_with_object({}) do |project_attribute, out|
            value = parse_project_attribute(project_attribute, parsed_content)
            out[project_attribute] = value unless value.nil?
            out
          end
        end

        # rubocop:disable Metrics/MethodLength
        # rubocop:disable Metrics/AbcSize
        def parse_project_attribute(project_attribute, parsed_content)
          new_parsed_content = parsed_content.clone
          new_parsed_content.css("br").each { |node| node.replace("\n") }
          new_parsed_content.css("script").each { |node| node.replace("") }
          selector = configuration["project_#{project_attribute}_selector".to_sym]
          unless selector.nil?
            elements = new_parsed_content.css(selector)
            # value = elements.last.attributes['href'].value rescue nil
            value = elements.map(&:text).reject(&:empty?)
                            .map(&:strip)
                            .map { |t| t.delete("\t") }
                            .map { |t| t.gsub("\r\n", "\n") }
                            .map { |t| t.gsub(/^$\n/, '') }
                            .join(', ') rescue nil

            value ||= elements.map { |e| t.attributes['src'].value rescue nil }.first
          end
          if methods.include?("extract_#{project_attribute}".to_sym)
            value ||= send("extract_#{project_attribute}", new_parsed_content)
          end

          value
        end

        # Creates project url based on the given project url
        # buy adding the base ulr if project url is a relative one
        #
        # @param project_url [String] extracted project url
        # @return [String] complete project url
        def build_project_url(project_url)
          return project_url if project_url =~ /^(https?|ftp):/
          configuration[:base_url] + '/' + project_url
        end

        def extract_email_from(attribute, parsed_content)
          content = parse_project_attribute(attribute, parsed_content)
          return unless content
          content.scan(/\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b/i).first
        end
      end
    end
  end
end
