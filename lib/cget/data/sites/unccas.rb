# http://www.unccas.org/-banque-d-experience-?debut_articles=10#pagination_articles
module Cget
  module Data
    module Sites
      class Unccas < Base
        @configuration = {
          base_url: 'http://www.unccas.org/-banque-d-experience-',
          project_url_selector: '[itemtype="https://schema.org/Article"] > a[itemprop="url"]',
          max_page_selector: '#pagination_articles + div ul.pagination > li:nth-last-child(2)',
          project_title_selector: '[itemtype="https://schema.org/Article"] [itemprop="headline"]',
          project_description_selector: '[itemtype="https://schema.org/Article"] [itemprop="articleBody"]',
          project_tags_selector: '[itemtype="https://schema.org/Article"] [itemprop="keywords"]',
          project_city_selector: '[itemtype="https://schema.org/Article"] [itemprop="contentLocation"]'
        }

        protected

        # Creates project url based on the given project url
        # buy adding the base ulr if project url is a relative one
        #
        # @param project_url [String] extracted project url
        # @return [String] complete project url
        def build_project_url(project_url)
          return project_url if project_url =~ /^(https?|ftp):/
          'http://www.unccas.org/' + project_url
        end

        def page_url
          configuration[:base_url] + "?debut_articles=#{(@current_page - 1) * 10}"
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          return unless title
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_email(parsed_content)
          extract_email_from(:contact, parsed_content) ||
          extract_email_from(:contact_html, parsed_content) ||
          extract_email_from(:address, parsed_content) ||
          extract_email_from(:address_html, parsed_content) ||
          extract_email_from(:description, parsed_content)
        end

        def extract_address(parsed_content)
          new_parsed_content = parsed_content.clone
          new_parsed_content.css("br").each { |node| node.replace("\n") }

          selector = '.col_gauche_fiches_exp > aside > ul > li article > a > div.sprite.like-br'
          parent = new_parsed_content.css(selector).first
          return unless parent
          elements = parent.parent.parent.css('.content') rescue nil
          return unless elements
          elements.map(&:text).reject(&:empty?)
                  .map(&:strip)
                  .map { |t| t.delete("\t") }
                  .map { |t| t.gsub("\r\n", "\n") }
                  .map { |t| t.gsub(/^$\n/, '') }
                  .join(', ') rescue nil
        end

        def extract_contact(parsed_content)
          new_parsed_content = parsed_content.clone
          new_parsed_content.css("br").each { |node| node.replace("\n") }

          selector = '.col_gauche_fiches_exp > aside > ul > li article > a > div.sprite.star-br'
          parent = new_parsed_content.css(selector).first
          return unless parent
          elements = parent.parent.parent.css('.content') rescue nil
          return unless elements
          elements.map(&:text).reject(&:empty?)
                  .map(&:strip)
                  .map { |t| t.delete("\t") }
                  .map { |t| t.gsub("\r\n", "\n") }
                  .map { |t| t.gsub(/^$\n/, '') }
                  .join(', ') rescue nil
        end

        def extract_address_html(parsed_content)
          new_parsed_content = parsed_content.clone

          selector = '.col_gauche_fiches_exp > aside > ul > li article > a > div.sprite.like-br'
          parent = new_parsed_content.css(selector).first
          return unless parent
          elements = parent.parent.parent.css('.content') rescue nil
          return unless elements
          elements.map(&:to_s).join(' ') rescue nil
        end

        def extract_contact_html(parsed_content)
          new_parsed_content = parsed_content.clone
          new_parsed_content.css("br").each { |node| node.replace("\n") }

          selector = '.col_gauche_fiches_exp > aside > ul > li article > a > div.sprite.star-br'
          parent = new_parsed_content.css(selector).first
          return unless parent
          elements = parent.parent.parent.css('.content') rescue nil
          return unless elements
          elements.map(&:to_s).join(' ') rescue nil
        end
      end
    end
  end
end
