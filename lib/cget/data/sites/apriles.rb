module Cget
  module Data
    module Sites
      class Apriles < Base
        @configuration = {
          base_url: 'http://www.apriles.net/index.php?option=com_frontpage&Itemid=1&limit=7',
          project_url_selector: '.content .contentpaneopen a:contains("Lire la suite")',
          project_title_selector: '.content h1',
          project_description_selector: '#sobi2outer',
          project_tags_selector: 'ul.sobi2Listing_field_type_action li',
          project_city_selector: '#sobi2Details_field_region'
        }

        protected

        # Creates project url based on the given project url
        # buy adding the base ulr if project url is a relative one
        #
        # @param project_url [String] extracted project url
        # @return [String] complete project url
        def build_project_url(project_url)
          return project_url if project_url =~ /^(https?|ftp):/
          'http://www.apriles.net/' + project_url
        end

        def max_pages
          31
        end

        def page_url
          configuration[:base_url] + "/&limitstart=#{(@current_page - 1) * 7}"
        end

        def extract_contact(parsed_content)
          new_parsed_content = parsed_content.clone
          new_parsed_content.css("br").each { |node| node.replace("\n") }

          selector = '#sobi2Listing_field_porteur_label:contains("Contact action") + a'
          link = new_parsed_content.css(selector).first
          return unless link
          url = link.attributes['href'].value rescue nil
          return unless url
          contact_content = extract_url_content(build_project_url(url))
          parsed_contact_content = Nokogiri::HTML(contact_content)
          parsed_contact_content.css("br").each { |node| node.replace("\n") }

          selector = '.cbFields'
          elements = parsed_contact_content.css(selector)

          elements.map(&:text).reject(&:empty?)
                  .map(&:strip)
                  .map { |t| t.delete("\t") }
                  .map { |t| t.gsub("\r\n", "\n") }
                  .map { |t| t.gsub(/^$\n/, '') }
                  .join(', ') rescue nil
        end

        def extract_address(parsed_content)
          parse_project_attribute(:contact, parsed_content)
        end

        def extract_email(parsed_content)
          extract_email_from(:contact, parsed_content) ||
          extract_email_from(:address, parsed_content) ||
          extract_email_from(:description, parsed_content)
        end

        def extract_zipcode(parsed_content)
          contact = parse_project_attribute(:contact, parsed_content)
          return unless contact
          match = contact.match(/[\n, ,^]([0-9]{5})[\n, ]/)
          return unless match
          match.values_at(1).first
        end
      end
    end
  end
end
