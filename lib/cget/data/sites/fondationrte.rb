# http://www.fondation-rte.org/actions/rechercher-un-projet/page/2/
module Cget
  module Data
    module Sites
      class Fondationrte < Base
        @configuration = {
          base_url: 'http://www.fondation-rte.org/actions/rechercher-un-projet/',
          project_url_selector: '.entry-title > a',
          project_title_selector: '.entry-title',
          project_description_selector: '.project-content',
          project_picture_selector: '#project-slideshow img:first-child'
        }

        protected

        def max_pages
          33
        end

        def page_url
          configuration[:base_url] + "page/#{@current_page}/"
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:title, parsed_content)
          return unless title
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/([0-9]{2})/)
          return unless match
          value = match.values_at(1).first
          value += "000" if value.size == 2
          value
        end

        def extract_email(parsed_content)
          extract_email_from(:description, parsed_content)
          extract_email_from(:contact, parsed_content)
        end

      end
    end
  end
end
