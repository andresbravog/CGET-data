# http://www.lelabo-ess.org/-Initiatives-inspirantes-.html?debut_articles2=10#pagination_articles2
module Cget
  module Data
    module Sites
      class Lelaboess < Base
        @configuration = {
          base_url: 'http://www.lelabo-ess.org/',
          project_url_selector: '.menu_articles > ul > li > a',
          project_title_selector: '[class*="article-titre-"]',
          project_description_selector: '.content',
          project_picture_selector: '[class*="article-chapo-"] > img'
        }

        protected

        def max_pages
          10
        end

        def page_url
          configuration[:base_url] + "-Initiatives-inspirantes-.html?debut_articles2=#{(@current_page - 1) * 10}#pagination_articles2"
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          return unless title
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_email(parsed_content)
          extract_email_from(:description, parsed_content)
          extract_email_from(:contact, parsed_content)
        end

      end
    end
  end
end
