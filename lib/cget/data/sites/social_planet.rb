# http://community.social-planet.org/hjform/all/139710
module Cget
  module Data
    module Sites
      class SocialPlanet < Base
        @configuration = {
          base_url: 'http://community.social-planet.org/hjform/all/139710',
          project_url_selector: '.elgg-item .elgg-body h3 > a',
          project_title_selector: '.elgg-main .elgg-body h2',
          project_description_selector: '.elgg-main .elgg-body span:contains("Résumé") + div',
          project_email_selector: '.elgg-main .elgg-body span:contains("E-mail") + div',
          project_contact_selector: '.elgg-main .elgg-body span:contains("Structure") + div',
          project_address_selector: '.elgg-main .elgg-body span:contains("Adresse") + div',
          project_tags_selector: '.elgg-main .elgg-body span:contains("Secteur") + div, .elgg-tag',
          project_city_selector: '.elgg-main .elgg-body span:contains("Adresse") + div'
        }

        protected

        def max_pages
          41
        end

        def page_url
          configuration[:base_url] + "?offset=#{(@current_page - 1) * 10}"
        end

        def extract_email(parsed_content)
          extract_email_from(:contact, parsed_content) ||
          extract_email_from(:address, parsed_content) ||
          extract_email_from(:description, parsed_content)
        end

        def extract_zipcode(parsed_content)
          title = parse_project_attribute(:address, parsed_content)
          match = title.match(/ ([0-9]{5}) /)
          match ||= title.match(/^([0-9]{5}) /)
          return unless match
          match.values_at(1).first
        end

        def extract_lat(parsed_content)
          new_parsed_content = parsed_content.clone
          new_parsed_content.css("br").each { |node| node.replace("\n") }

          selector = 'a.carto_link'
          link = new_parsed_content.css(selector).first
          return unless link
          url = link.attributes['href'].value rescue nil
          return unless url
          url.scan(/[\&,^,\/,\?]lat\=([0-9,\.]{2,15})[\&,$]/).flatten.first
        end

        def extract_long(parsed_content)
          new_parsed_content = parsed_content.clone
          new_parsed_content.css("br").each { |node| node.replace("\n") }

          selector = 'a.carto_link'
          link = new_parsed_content.css(selector).first
          return unless link
          url = link.attributes['href'].value rescue nil
          return unless url
          url.scan(/[\&,^,\/,\?]lng\=([0-9,\.]{2,15})[\&,$]/).flatten.first
        end
      end
    end
  end
end
