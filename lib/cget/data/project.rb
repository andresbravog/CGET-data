module Cget
  module Data
    class Project
      attr_accessor :title, :link, :tags, :description, :site, :email, :contact, :address, :city, :zipcode,
                    :lat, :long, :picture

      REQUIRED_ATTRS = %I{title link site}.freeze

      def initialize(given_attrs = {})
        given_attrs.keys.map { |key| send("#{key}=", given_attrs[key]) }
      end

      def valid?
        REQUIRED_ATTRS.each { |attribute| return false if send(attribute).empty? }
        true
      end
    end
  end
end
