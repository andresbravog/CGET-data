require 'spec_helper'

describe Cget::Data do
  it 'has a version number' do
    expect(Cget::Data::VERSION).not_to be nil
  end

  describe '#find' do
    use_vcr_cassette 'data_find'

    let(:limit) { 5 }
    let(:sites) { [:resolis] }
    let(:params) { { sites: sites, limit: limit } }
    subject { Cget::Data.find(params) }

    describe 'resolis' do
      let(:sites) { [:resolis] }

      # context 'with no params given' do
      #   let(:limit) { 5000 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for all available sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('Développement de l\'énergie solaire au Bangladesh par la Grameen Shakti') }
        its(:tags) { should eql('Source d’inspiration !, Développement territorial, Environnement') }
        its(:description) { should eql('Le programme Solar Home System de l’association bangladeshi Grameen Shkati vise à installer des systèmes d’approvisionnement en électricité solaire dans les foyers ruraux, et apporte donc un soutien technique et financier (via les micro-crédits) aux femmes.') }
        its(:email) { should eql('mmahmodul.hasan@yahoo.com') }
        its(:contact) { should eql('Grameen Bank Bhaban (19e étage), Mirpur -2, 1216 Dhaka') }
        its(:address) { should eql('Grameen Bank Bhaban (19e étage), Mirpur -2, 1216 Dhaka') }
        its(:city) { should eql('Bangladesh') }
        its(:zipcode) { should eql(nil) }
        its(:link){ should eql('http://www.resolis.org/consulter-les-pratiques-locales/solution/agriculture-et-alimentation+coordination-des-actions+culture-sport-et-loisirs+democratie-et-bonne-gouvernance+developpement-territorial+economie-solidaire+education+emploi+environnement+exclusion-et-isolement+logement+mobilite+precarite-energetique+sante/../../../fiche-pratique/developpement-de-l-energie-solaire-au-bangladesh-par-la-grameen-shakti/929') }
        its(:site){ should eql('Cget::Data::Sites::Resolis') }
      end
    end

    describe 'foundation_vinci' do
      let(:sites) { [:fondation_vinci] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('20ans 1 Projet, Département AJA de l\'hôpital Saint Louis') }
        its(:tags) { should eql(nil) }
        its(:description) { should match('1 En France, plus de 4000 jeunes adultes de 18 à 25 ans souffrent de maladies graves dont plus de 2700 atteints de cancer. Ces maladies demandent des traitements longs, quelquefois sur une durée d’un à trois ans') }
        its(:email) { should eql('') }
        its(:contact) { should match('32 rue de Tocqueville') }
        its(:address) { should match('32 rue de Tocqueville') }
        its(:city) { should eql('Île-de-France - Paris') }
        its(:zipcode) { should eql('75017') }
        its(:link){ should eql('https://www.fondation-vinci.com/fondation/fr/nos-projets/pages/20ans_1_projet_departement_aja_de_l_hopital_saint_louis_paris_france_5540.htm') }
        its(:site){ should eql('Cget::Data::Sites::FondationVinci') }
      end
    end

    describe 'fondation_veolia' do
      let(:sites) { [:fondation_veolia] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('Des filtres à sable domestiques pour supprimer l’arsenic') }
        its(:tags) { should eql(nil) }
        its(:description) { should match('Ce projet est exemplaire sur plusieurs points.  Premièrement, il est scientifiquement éprouvé : les étudiants ont testé plusieurs types de filtres et ont mis en place un programme de suivi rigoureux') }
        its(:email) { should eql(nil) }
        its(:contact) { should match(nil) }
        its(:address) { should match(nil) }
        its(:city) { should eql('Villages de Liangjiabu et Zuojiabu dans la province de Shanxi (près de 600 km au sud de Pékin)') }
        its(:zipcode) { should eql(nil) }
        its(:link){ should eql('http://www.fondation.veolia.com/fr/des-filtres-sable-domestiques-pour-supprimer-l-arsenic') }
        its(:site){ should eql('Cget::Data::Sites::FondationVeolia') }
      end
    end

    describe 'imagination_for_people' do
      let(:sites) { [:imagination_for_people] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('nLzbE') }
        its(:tags) { should eql(nil) }
        its(:description) { should match('None') }
        its(:email) { should eql(nil) }
        its(:contact) { should eql('') }
        its(:address) { should eql('') }
        its(:city) { should eql('') }
        its(:zipcode) { should eql(nil) }
        its(:link){ should eql('http://imaginationforpeople.org/fr/project/nlzbe/') }
        its(:site){ should eql('Cget::Data::Sites::ImaginationForPeople') }
      end
    end

    describe 'unccas' do
      let(:sites) { [:unccas] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('Projet Social de Territoire de Bondy') }
        its(:tags) { should eql('analyse des besoins sociaux, politique de la ville') }
        its(:description) { should match('Mise en œuvre, à la suite de l’ABS, d’une démarche de projet social de territoire qui modifie en profondeur la gouvernance des politiques territoriales pour favoriser sur le long terme une élaboration et une évaluation partagées de l’action sociale avec les partenaires et améliorer la coordination et la complémentarité.') }
        its(:email) { should eql('e.platteau@ville-bondy.fr') }
        its(:contact) { should match('Emmanuelle PLATTEAU') }
        its(:address) { should match('15, place Albert Thomas') }
        its(:city) { should eql('Île-de-France') }
        its(:zipcode) { should eql('93143') }
        its(:link){ should eql('http://www.unccas.org/projet-social-de-territoire-de-bondy') }
        its(:site){ should eql('Cget::Data::Sites::Unccas') }
      end
    end

    describe 'experimentation_jeunes' do
      let(:sites) { [:experimentation_jeunes] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('3D FabLab') }
        its(:tags) { should eql('Education populaire') }
        its(:description) { should match('Ce projet envisage la création d’un FabLab ouvert à tous dans le domaine de l’art, de la communication et de l’imagerie 3D avec mise à disposition du matériel et de l’expertise de l’association pour permettre aux jeunes de développer leurs propres médias.') }
        its(:email) { should eql(nil) }
        its(:contact) { should eql(nil) }
        its(:address) { should eql(nil) }
        its(:city) { should eql('Île-de-France') }
        its(:zipcode) { should eql(nil) }
        its(:link){ should eql('http://www.experimentation.jeunes.gouv.fr/spip.php?page=article&id_article=1372') }
        its(:site){ should eql('Cget::Data::Sites::ExperimentationJeunes') }
      end
    end

    describe 'apriles' do
      let(:sites) { [:apriles] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('Le parrainage de proximité se donne du PEPSE') }
        its(:tags) { should eql('Education, Relations de voisinage, Liens intergénérationnels, Lutte contre l\'exclusion sociale, Protection de l\'enfance, Parentalité, Jeunesse, Gouvernance, partenariats institutionnels, Nouvelles pratiques professionnelles, Participation des habitants, Bénévolat') }
        its(:description) { should match('Afin de développer de nouvelles formes de parrainage') }
        its(:email) { should eql(nil) }
        its(:contact) { should match('39 Bd Demorieux') }
        its(:address) { should match('39 Bd Demorieux') }
        its(:city) { should eql('Région :  Pays de la Loire') }
        its(:zipcode) { should eql('72100') }
        its(:link){ should eql('http://www.apriles.net/index.php?option=com_sobi2&sobi2Task=sobi2Details&catid=5&sobi2Id=1590&Itemid=95') }
        its(:site){ should eql('Cget::Data::Sites::Apriles') }
      end
    end

    describe 'social_planet' do
      let(:sites) { [:social_planet] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('ALTERNATIV\'HOTEL') }
        its(:tags) { should eql('action sociale et insertion, innovation 2015, essaimage, Alternativ\'hôtel, hébergement, hôtel, alternative, alternativ\'hôtel, urgence, familles, Auvergne') }
        its(:description) { should match('Comme son nom l’indique notre initiative est une alternative à l’hôtel comme solution de l’hébergement d’urgence. L’hôtel est cher, mal adapté au travail social et aux familles') }
        its(:email) { should eql('als.team@outlook.fr') }
        its(:contact) { should match('ATELIER LOGEMENT SOLIDAIRE') }
        its(:address) { should match('11 Rue Marmontel, 63000 Clermont-Ferrand, France') }
        its(:city) { should eql('11 Rue Marmontel, 63000 Clermont-Ferrand, France') }
        its(:zipcode) { should eql('63000') }
        its(:link){ should eql('http://community.social-planet.org/hjform/view/286572/alternativhotel') }
        its(:site){ should eql('Cget::Data::Sites::SocialPlanet') }
        its(:lat){ should eql('45.7684168') }
        its(:long){ should eql('3.0825849') }
      end
    end

    describe 'semeoz' do
      let(:sites) { [:semeoz] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('  Ian Motion') }
        its(:tags) { should eql(%{Énergie, Transports, Energies renouvelables, Récup}) }
        its(:description) { should match(%{Créée en juin dernier par quatre ingénieurs de l'industrie automobile, Ian Motion – c'est son nom – a l'ambition de "donner une seconde vie aux véhicules afin d'éviter leur mise à la casse", explique Laurent Blond, l'un de ses cofondateurs. Pour lui, jeter une voiture fonctionnelle afin d'acheter un véhicule électrique n'a "aucun sens d'un point de vue écologique" car la fabrication de ce dernier émettra entre 6 et 8 tonnes de CO2, sans compter le gaspillage de matériaux.}) }
        its(:email) { should eql(nil) }
        its(:contact) { should eql(nil) }
        its(:address) { should eql(nil) }
        its(:city) { should eql(nil) }
        its(:zipcode) { should eql(nil) }
        its(:link){ should eql('http://semeoz.info/annuaire/ian-motion/') }
        its(:site){ should eql('Cget::Data::Sites::Semeoz') }
        its(:lat){ should eql(nil) }
        its(:long){ should eql(nil) }
      end
    end

    describe 'bretagne_creative' do
      let(:sites) { [:bretagne_creative] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('"Prends soin de ta mer"') }
        its(:description) { should match(%{\nParticipez à notre projet de création d’un livre sur la préservation}) }
        its(:email) { should eql(nil) }
        its(:contact) { should eql(%{Informations\nAuteurs\nnathalie chaline\nMaturité\nNon renseigné\nSite Web\nMise à jour\n31 mai 2016\nThèmes\nÉducation, Environnement, Jeunesse\nPays\nPays de Brest\nLicence\n CC by-sa\n \nContacter les porteurs du projet}) }
        its(:address) { should eql(nil) }
        its(:city) { should eql(nil) }
        its(:zipcode) { should eql(nil) }
        its(:link){ should eql('http://www.bretagne-creative.net//article1148.html') }
        its(:site){ should eql('Cget::Data::Sites::BretagneCreative') }
        its(:lat){ should eql(nil) }
        its(:long){ should eql(nil) }
      end
    end

    describe 'bleublanczebre' do
      let(:sites) { [:bleublanczebre] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('BLABLA  JOB') }
        its(:description) { should match(%{une Région jeune mais fortement touchée par le chômage du public jeune}) }
        its(:email) { should eql(nil) }
        its(:contact) { should eql(nil) }
        its(:address) { should eql(nil) }
        its(:city) { should eql(nil) }
        its(:zipcode) { should eql(nil) }
        its(:link){ should eql('http://www.bleublanczebre.fr/je-decouvre/les-actions/blabla-job') }
        its(:site){ should eql('Cget::Data::Sites::Bleublanczebre') }
        its(:lat){ should eql(nil) }
        its(:long){ should eql(nil) }
      end
    end

    describe 'economiecirculaire' do
      let(:sites) { [:economiecirculaire] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('Une filière de recyclage des vêtements de pompiers proposée par KERMEL') }
        its(:description) { should match(%{l’éco-conception aboutit à la création de la première filière de recyclage}) }
        its(:email) { should eql(nil) }
        its(:contact) { should eql('20 Rue Ampère, 68000 Colmar') }
        its(:address) { should eql('20 Rue Ampère, 68000 Colmar') }
        its(:city) { should eql(nil) }
        its(:zipcode) { should eql('68000') }
        its(:link){ should eql('http://www.economiecirculaire.org/initiative/h/une-filiere-de-recyclage-des-vetements-de-pompiers-proposee-par-kermel.html') }
        its(:site){ should eql('Cget::Data::Sites::Economiecirculaire') }
        its(:lat){ should eql('7.3738652') }
        its(:long){ should eql('48.0845115') }
      end
    end

    describe 'fondationrte' do
      let(:sites) { [:fondationrte] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('« Et pourtant elle tourne » : projet d’alimentation générale culturelle (58 – Nièvre)') }
        its(:description) { should match(%{Les villages et hameaux de la Nièvre et du Morvan voient aujourd’hui se raréfier les services publics de proximité, les activités économiques, les médecins, les transports collectifs tandis}) }
        its(:email) { should eql(nil) }
        its(:contact) { should eql(nil) }
        its(:address) { should eql(nil) }
        its(:city) { should eql(nil) }
        its(:zipcode) { should eql('58000') }
        its(:link){ should eql('http://www.fondation-rte.org/projet/et-pourtant-elle-tourne-projet-dalimentation-generale-culturelle-58-nievre/') }
        its(:site){ should eql('Cget::Data::Sites::Fondationrte') }
        its(:lat){ should eql(nil) }
        its(:long){ should eql(nil) }
      end
    end

    describe 'lelaboess' do
      let(:sites) { [:lelaboess] }

      # context 'with no params given' do
      #   let(:limit) { 5 }
      #   it 'retrieves projects for all available sites' do
      #     expect(subject.size).to eq(621)
      #   end
      #
      #   it 'returns a list of Projects' do
      #     expect(subject.first).to be_a(Cget::Data::Project)
      #   end
      #
      #   it 'returns a list of valid Projects' do
      #     expect(subject.first).to be_valid
      #   end
      # end

      context 'with limit param given' do
        let(:limit) { 5 }

        it 'retrieves projects for given sites' do
          expect(subject.size).to eq(5)
        end

        it 'returns a list of Projects' do
          expect(subject.first).to be_a(Cget::Data::Project)
        end

        it 'returns a list of valid Projects' do
          expect(subject.first).to be_valid
        end
      end

      describe 'project_data' do
        subject { Cget::Data.find(params).first }
        its(:title) { should eql('U2 GUIDE la plateforme collaborative pour changer le monde en voyageant') }
        its(:description) { should match(%{Lancée en 2014, U2GUIDE.com est un site de voyage collaboratif et solidaire mettant en relation guides locaux}) }
        its(:email) { should eql(nil) }
        its(:contact) { should eql(nil) }
        its(:address) { should eql(nil) }
        its(:city) { should eql(nil) }
        its(:zipcode) { should eql(nil) }
        its(:link){ should eql('http://www.lelabo-ess.org//u2-guide-la-plateforme-collaborative-pour-changer.html') }
        its(:site){ should eql('Cget::Data::Sites::Lelaboess') }
        its(:lat){ should eql(nil) }
        its(:long){ should eql(nil) }
      end
    end
  end
end
