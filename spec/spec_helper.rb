$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)
require 'cget/data'
require 'webmock'
require 'vcr'
require 'pry'
require 'rspec/its'

VCR.configure do |c|
  c.cassette_library_dir = 'spec/fixtures/vcr_cassettes'
  c.hook_into :webmock
  c.default_cassette_options = { record: :new_episodes }
  c.allow_http_connections_when_no_cassette = true
end

RSpec.configure do |config|
  config.extend VCR::RSpec::Macros
end
